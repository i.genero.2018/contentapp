#Raíz para la jerarquía de clases que implementan aplicaciones web.

import socket

class webApp:
    def parse (self, request):
        print("Parse: Analizando la petición recibida del navegador")
        return None
    def process (self, parsedRequest):
        """Recibe como argumento el objeto con lo analizado por el método anterior, y
        devolverá una lista con el código resultante y la página HTML a devolver"""
        print("Process: Te devuelvo lo que has pedido")
        return ("200 OK", "<html><body><h1>Esto es una páina web</h1></body></html>")

    def __init__ (self, hostname, port):
        """Código para inicializar una instancia que incluye el bucle general de atención
         a clientes, y la gestión de sockets necesaria para que funcione."""
        mySocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))
        mySocket.listen(5)

        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received (going to parse and process):")
            request = recvSocket.recv(2048)
            print(request)

            #Llama al método parse para analizar la petición recibida
            parsedRequest = self.parse(request.decode('utf8'))

            #Llama al método process para procesar la petición analizada
            (returnCode, htmlAnswer) = self.process(parsedRequest)

            print("Answering back...")
            response = "HTTP/1.1 " + returnCode + " \r\n\r\n" \
                       + htmlAnswer + "\r\n"
            recvSocket.send(response.encode('utf8'))
            recvSocket.close()

if __name__ == "__main__":
    testWebApp = webApp("localhost", 1234)