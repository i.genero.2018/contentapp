"""Programa que sirve el contenido almacenado en un diccionario Python.
La clave del diccionario es el nombre de recurso a servir, y el valor
es el cuerpo de la página HTML correspondiente a ese recurso."""

import webapp #Importa la clase donde se gestiona el funcionamiento del servidor

#Páginas de respuesta
PAGE_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""

class ContentApp(webapp.webApp): #Clase que hereda de la clase webApp

    diccionario = {'/': "<p>Main page</p>",
                '/hola': "<p>Hola mundo</p>",
                '/adios': "<p>Adios mundo cruel</p>"}

    def parse (self, request):
        """Analiza la petición recibida del navegador
        seleccionando el segundo elemento de la lista
        que es la url del recurso solicitado."""
        return request.split(' ',2)[1] #

    def process (self, resource):
        """Si el recurso solicitado es una de las 3 opciones que hay en el diccionario
        se devuelve el contenido correspondiente, si no se devuelve un mensaje de error."""
        if resource in self.diccionario:
            content = self.diccionario[resource]
            page = PAGE_FOUND.format(content=content)
            code = "200 OK"
        else:
            page = PAGE_NOT_FOUND.format(resource=resource)
            code = "404 Resource Not Found"
        return (code, page)

if __name__ == "__main__":
    webApp = ContentApp ("localhost", 1234)